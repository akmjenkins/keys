<?php $bodyclass = ''; ?>
<?php include('inc/i-header.php'); ?>

<div class="hero">
	<div class="hero-wrap">
	
		<div class="hero-slide" data-src="../assets/dist/images/temp/hero/hero-1.jpg">
			<div class="hero-item" style="background-image: url(../assets/dist/images/temp/hero/hero-1.jpg);"></div>
			
			<div class="hero-content d-bg">
				<h1 class="hero-title">News</h1>
				<span class="hero-subtitle">Keep up to date</span>
				
			</div><!-- .hero-content -->
			
		</div>
		
		<div class="hero-slide" data-src="../assets/dist/images/temp/hero/hero-2.jpg">
			<div class="hero-item" style="background-image: url(../assets/dist/images/temp/hero/hero-2.jpg);"></div>
		</div>
		
		<div class="hero-slide" data-src="../assets/dist/images/temp/hero/hero-3.jpg">
			<div class="hero-item" style="background-image: url(../assets/dist/images/temp/hero/hero-3.jpg);"></div>
		</div>
		
		<div class="hero-slide" data-src="../assets/dist/images/temp/hero/hero-4.jpg">
			<div class="hero-item" style="background-image: url(../assets/dist/images/temp/hero/hero-1.jpg);"></div>
		</div>

		
	</div><!-- .hero-wrap -->
	
</div><!-- .hero -->

<div class="sw full">
	<div class="body">
		
		<section>
		
			<blockquote class="center">
				Find out what’s happening with Spare Keys and more as we post news items related to our services and other things.
			</blockquote>
		
			<div class="filter-section">
			
				<div class="filter-bar">
				
					<div class="filter-bar-meta">
					
						<div class="filter-controls">
							<button class="previous">Prev</button>
							<button class="next">Next</button>
						</div><!-- .filter-controls -->
					
						<form action="/" method="post" class="search-form single-form">
							<fieldset>
								<input type="text" name="s" placeholder="Search events...">
								<button class="t-fa-abs fa-search">Search</button>
							</fieldset>
						</form>
					
					</div>
				
					<div class="count">
						<span class="num">10</span> Articles
					</div><!-- .count -->
					
				</div><!-- .filter-bar -->
				
				<div class="filter-contents">
				
					<div class="grid eqh latest-grid">
						<div class="col col-3 sm-col-2 xs-col-1">
							<div class="item">
								<div class="pad-20">
									
									<time datetime="2014-05-24" class="t">
										<span class="t-day">24</span>
										<span class="t-month-year">
											<span class="t-month">May</span>
											<span class="t-year">2014</span>
										</span>
									</time>
									
									<div class="hgroup">
										<span class="h4-style">Latest News Title Here</span>
										<span class="subtitle">Subtitle Here</span>
									</div><!-- .hgroup -->
									
									<p>
										Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. 
										Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. 
										Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem.
									</p>
									
									<div class="latest-button">
										<a href="#" class="button">Read More</a>
									</div><!-- .latest-button -->
									
								</div>
							</div><!-- .item -->
						</div><!-- .col -->
						<div class="col col-3 sm-col-2 xs-col-1">
							<div class="item">
								<div class="pad-20">
									
									<time datetime="2014-05-24" class="t">
										<span class="t-day">24</span>
										<span class="t-month-year">
											<span class="t-month">May</span>
											<span class="t-year">2014</span>
										</span>
									</time>
									
									<div class="hgroup">
										<span class="h4-style">Latest News Title Here</span>
										<span class="subtitle">Subtitle Here</span>
									</div><!-- .hgroup -->
									
									<p>
										Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. 
										Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. 
										Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem.
									</p>
									
									<div class="latest-button">
										<a href="#" class="button">Read More</a>
									</div><!-- .latest-button -->
									
								</div>
							</div><!-- .item -->
						</div><!-- .col -->
						<div class="col col-3 sm-col-2 xs-col-1">
							<div class="item">
								<div class="pad-20">
									
									<time datetime="2014-05-24" class="t">
										<span class="t-day">24</span>
										<span class="t-month-year">
											<span class="t-month">May</span>
											<span class="t-year">2014</span>
										</span>
									</time>
									
									<div class="hgroup">
										<span class="h4-style">Latest News Title Here</span>
										<span class="subtitle">Subtitle Here</span>
									</div><!-- .hgroup -->
									
									<p>
										Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. 
										Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. 
										Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem.
									</p>
									
									<div class="latest-button">
										<a href="#" class="button">Read More</a>
									</div><!-- .latest-button -->
									
								</div>
							</div><!-- .item -->
						</div><!-- .col -->
						<div class="col col-3 sm-col-2 xs-col-1">
							<div class="item">
								<div class="pad-20">
									
									<time datetime="2014-05-24" class="t">
										<span class="t-day">24</span>
										<span class="t-month-year">
											<span class="t-month">May</span>
											<span class="t-year">2014</span>
										</span>
									</time>
									
									<div class="hgroup">
										<span class="h4-style">Latest News Title Here</span>
										<span class="subtitle">Subtitle Here</span>
									</div><!-- .hgroup -->
									
									<p>
										Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. 
										Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. 
										Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem.
									</p>
									
									<div class="latest-button">
										<a href="#" class="button">Read More</a>
									</div><!-- .latest-button -->
									
								</div>
							</div><!-- .item -->
						</div><!-- .col -->
						<div class="col col-3 sm-col-2 xs-col-1">
							<div class="item">
								<div class="pad-20">
									
									<time datetime="2014-05-24" class="t">
										<span class="t-day">24</span>
										<span class="t-month-year">
											<span class="t-month">May</span>
											<span class="t-year">2014</span>
										</span>
									</time>
									
									<div class="hgroup">
										<span class="h4-style">Latest News Title Here</span>
										<span class="subtitle">Subtitle Here</span>
									</div><!-- .hgroup -->
									
									<p>
										Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. 
										Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. 
										Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem.
									</p>
									
									<div class="latest-button">
										<a href="#" class="button">Read More</a>
									</div><!-- .latest-button -->
									
								</div>
							</div><!-- .item -->
						</div><!-- .col -->
						<div class="col col-3 sm-col-2 xs-col-1">
							<div class="item">
								<div class="pad-20">
									
									<time datetime="2014-05-24" class="t">
										<span class="t-day">24</span>
										<span class="t-month-year">
											<span class="t-month">May</span>
											<span class="t-year">2014</span>
										</span>
									</time>
									
									<div class="hgroup">
										<span class="h4-style">Latest News Title Here</span>
										<span class="subtitle">Subtitle Here</span>
									</div><!-- .hgroup -->
									
									<p>
										Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. 
										Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. 
										Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem.
									</p>
									
									<div class="latest-button">
										<a href="#" class="button">Read More</a>
									</div><!-- .latest-button -->
									
								</div>
							</div><!-- .item -->
						</div><!-- .col -->
						
					</div><!-- .grid -->

				
				</div><!-- .filter-contents -->
				
			</div><!-- .filter-section -->
			
		</section>
		
	</div><!-- .body -->
</div><!-- .sw -->


<?php include('inc/i-footer.php'); ?>