<?php $bodyclass = ''; ?>
<?php include('inc/i-header.php'); ?>

<div class="hero">
	<div class="hero-wrap">
	
		<div class="hero-slide" data-src="../assets/dist/images/temp/hero/hero-1.jpg">
			<div class="hero-item" style="background-image: url(../assets/dist/images/temp/hero/hero-1.jpg);"></div>
			
			<div class="hero-content d-bg">
				<h1 class="hero-title">Why Spare Keys?</h1>
				<span class="hero-subtitle">Safe Secure Convenient</span>
				
			</div><!-- .hero-content -->
			
		</div>
		
		<div class="hero-slide" data-src="../assets/dist/images/temp/hero/hero-2.jpg">
			<div class="hero-item" style="background-image: url(../assets/dist/images/temp/hero/hero-2.jpg);"></div>
		</div>
		
		<div class="hero-slide" data-src="../assets/dist/images/temp/hero/hero-3.jpg">
			<div class="hero-item" style="background-image: url(../assets/dist/images/temp/hero/hero-3.jpg);"></div>
		</div>
		
		<div class="hero-slide" data-src="../assets/dist/images/temp/hero/hero-4.jpg">
			<div class="hero-item" style="background-image: url(../assets/dist/images/temp/hero/hero-1.jpg);"></div>
		</div>

		
	</div><!-- .hero-wrap -->
	
</div><!-- .hero -->

<div class="sw full">
	<div class="body">
		
		<section>
			<div class="article-body center">
			
				<span class="circle-graphic-wrap">
					<span class="circle-graphic lazybg" data-src="../assets/dist/images/temp/circle-graphics/call.png"></span>
				</span><!-- .circle-graphic-wrap -->
				
				<div class="section-title">
					<h2 class="title">Secure Storage</h2>
				</div><!-- .section-title -->
				
				<p>Upon receiving your keys, they will be catalogued and photographed based on your unique security code. 
				Once you have confirmed each set is yours, we will store them within tamper-proof envelopes in a fire-proof safe 
				located in our undisclosed location. Complete with an alarm system, CCTV, and 24-hour on-site security, 
				you can rest assured that your keys are safe.</p>
				  
				<p>Note: In case of an unlikely break-in, your keys will only be identifiable by the unique security code 
				they are associated with since all other info (address, first name, etc.) is stored electronically in a secure database. 
				Without access to this database, your keys are useless.</p>
				
			</div>
			
		</section>
		
		<hr />
		
		<section>
			<div class="article-body center">
			
				<span class="circle-graphic-wrap">
					<span class="circle-graphic lazybg" data-src="../assets/dist/images/temp/circle-graphics/clock.png"></span>
				</span><!-- .circle-graphic-wrap -->
				
				<div class="section-title">
					<h2 class="title">24 Hour Service</h2>
				</div><!-- .section-title -->
				
				<p>Since we understand that you may need your keys at any time, day or night, we will have a service 
				representative available to answer your requests 24 hours a day, seven days a week. 
				Depending on your preference, you may contact us via phone, email, or text.</p>
				
			</div>			
		</section>
		
		<hr />
		
		<section>
			<div class="article-body center">
			
				<span class="circle-graphic-wrap">
					<span class="circle-graphic lazybg" data-src="../assets/dist/images/temp/circle-graphics/guarantee.png"></span>
				</span><!-- .circle-graphic-wrap -->
				
				<div class="section-title">
					<h2 class="title">Guarantee</h2>
				</div><!-- .section-title -->
				
				<p>We offer our service to ensure that you don’t end up locked out in an emergency or anytime at all, and we work 
				hard to be a better option than a locksmith or another costly service. In that regard, we guarantee prompt delivery 
				whenever possible. Within the metro area, our delivery personnel will be able to quote you a 
				time upon confirmation of your identity, and if this time is not met, then a refund is offered.</p>
				 
				<p>Outside of the metro area, we will provide a number of options to ensure your keys reach you as soon as possible. 
				Depending on your needs, some of these options may incur an extra cost, but everything will be 
				discussed upon your request for your keys, and nothing will be charged without your approval.</p>
				
			</div>
		</section>
		
		<hr />
		
		<section>
			
			<?php include('inc/i-popular-questions.php'); ?>
			
		</section>
		
	</div><!-- .body -->
</div><!-- .sw -->


<?php include('inc/i-footer.php'); ?>