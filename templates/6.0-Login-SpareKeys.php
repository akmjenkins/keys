<?php $bodyclass = ''; ?>
<?php include('inc/i-header.php'); ?>

<div class="hero">
	<div class="hero-wrap">
	
		<div class="hero-slide" data-src="../assets/dist/images/temp/hero/hero-1.jpg">
			<div class="hero-item" style="background-image: url(../assets/dist/images/temp/hero/hero-1.jpg);"></div>
			
			<div class="hero-content d-bg">
				<h1 class="hero-title">Login</h1>
				<span class="hero-subtitle">Access uour information</span>
				
			</div><!-- .hero-content -->
			
		</div>
		
		<div class="hero-slide" data-src="../assets/dist/images/temp/hero/hero-2.jpg">
			<div class="hero-item" style="background-image: url(../assets/dist/images/temp/hero/hero-2.jpg);"></div>
		</div>
		
		<div class="hero-slide" data-src="../assets/dist/images/temp/hero/hero-3.jpg">
			<div class="hero-item" style="background-image: url(../assets/dist/images/temp/hero/hero-3.jpg);"></div>
		</div>
		
		<div class="hero-slide" data-src="../assets/dist/images/temp/hero/hero-4.jpg">
			<div class="hero-item" style="background-image: url(../assets/dist/images/temp/hero/hero-1.jpg);"></div>
		</div>

		
	</div><!-- .hero-wrap -->
	
</div><!-- .hero -->

<div class="sw full">
	<div class="body">
		
		<section>
			
			<blockquote class="center">
				Update your contact info and other important items through our customer login system. If you do not already have an account, please create one.
			</blockquote>
			
			<div class="grid eqh">
				<div class="col col-2 sm-col-1">
					<div class="item bordered-item">
						<div class="pad-40 sm-pad-20">
						
							<h2 class="uc">Existing User</h2>
							
							<form action="/" class="body-form">
								<fieldset>
									<input type="email" name="email" placeholder="Email Address">
									<input type="password" name="password" placeholder="Password">
									<a href="#" class="f-right inline">Forgot password?</a>
									<button class="button" type="submit">Log In</button>
								</fieldset>
							</form>
						
						</div>
					</div><!-- .item -->
				</div><!-- .col -->
				<div class="col col-2 sm-col-1">
					<div class="item bordered-item">
						<div class="pad-40 sm-pad-20">
						
							<h2 class="uc">New User</h2>
							
							<form action="/" class="body-form">
								<fieldset>
									<input type="email" name="email" placeholder="Email Address">
									<input type="password" name="password" placeholder="Password">
									<button class="button" type="submit">Sign Up</button>
								</fieldset>
							</form>
						
						</div>
					</div><!-- .item -->
				</div><!-- .col -->
			</div><!-- .grid -->
				
		</section>
		
	</div><!-- .body -->
</div><!-- .sw -->


<?php include('inc/i-footer.php'); ?>