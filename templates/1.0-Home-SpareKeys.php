<?php $bodyclass = 'home'; ?>
<?php include('inc/i-header.php'); ?>
<?php include('inc/i-home-hero.php'); ?>

<div class="sw full">
	<div class="body">
		
		<section>
			
			<?php include('inc/i-how-it-works-grid.php'); ?>
			
		</section>
		
		<section>
			
			<?php include('inc/i-plans-pricing.php'); ?>
			
		</section>
		
		<section>
			
			<?php include('inc/i-the-latest.php'); ?>
			
		</section>
		
	</div><!-- .body -->
</div><!-- .sw -->


<?php include('inc/i-footer.php'); ?>