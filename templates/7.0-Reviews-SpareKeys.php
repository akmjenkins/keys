<?php $bodyclass = ''; ?>
<?php include('inc/i-header.php'); ?>

<div class="hero">
	<div class="hero-wrap">
	
		<div class="hero-slide" data-src="../assets/dist/images/temp/hero/hero-1.jpg">
			<div class="hero-item" style="background-image: url(../assets/dist/images/temp/hero/hero-1.jpg);"></div>
			
			<div class="hero-content d-bg">
				<h1 class="hero-title">Reviews</h1>
				<span class="hero-subtitle">The right choice.</span>
				
			</div><!-- .hero-content -->
			
		</div>
		
		<div class="hero-slide" data-src="../assets/dist/images/temp/hero/hero-2.jpg">
			<div class="hero-item" style="background-image: url(../assets/dist/images/temp/hero/hero-2.jpg);"></div>
		</div>
		
		<div class="hero-slide" data-src="../assets/dist/images/temp/hero/hero-3.jpg">
			<div class="hero-item" style="background-image: url(../assets/dist/images/temp/hero/hero-3.jpg);"></div>
		</div>
		
		<div class="hero-slide" data-src="../assets/dist/images/temp/hero/hero-4.jpg">
			<div class="hero-item" style="background-image: url(../assets/dist/images/temp/hero/hero-1.jpg);"></div>
		</div>

		
	</div><!-- .hero-wrap -->
	
</div><!-- .hero -->

<div class="sw full">
	<div class="body">
		
		<section>
		
			<blockquote class="center">
				With a much needed service that comes at a great price, it’s no surprise that our customers are happy they chose Spare Keys!
			</blockquote>
		
			<div class="filter-section">
			
				<div class="filter-bar">
				
					<div class="filter-bar-meta">
					
						<div class="filter-controls">
							<button class="previous">Prev</button>
							<button class="next">Next</button>
						</div><!-- .filter-controls -->
					
					</div>
				
					<div class="count">
						<span class="num">10</span> Reviews
					</div><!-- .count -->
					
				</div><!-- .filter-bar -->
				
				<div class="filter-contents">
				
					<div class="grid eqh vcenter">
					
						<div class="col col-3 sm-col-2 xs-col-1">
							<div class="item dark-bg">
								<div class="pad-10 center testimonial">
									<blockquote>
									
										"Lorem ipsum dolor sit amet,
										consectetuer adipiscing elit. Aenean
										commodo ligula eget dolor. Aenean
										massa. Cum sociis natoque
										massa. Cum sociis natoque penatibus et
										magnis dis parturient montes."
									
										<br>
									
										<cite>John C.</cite>
									</blockquote>
								</div><!-- .pad-10 -->
							</div><!-- .item -->
						</div><!-- .col -->
						
						<div class="col col-3 sm-col-2 xs-col-1">
							<div class="item dark-bg">
								<div class="pad-10 center testimonial">
									<blockquote>
									
										"Lorem ipsum dolor sit amet,
										consectetuer adipiscing elit. Aenean
										commodo ligula eget dolor."
									
										<br>
									
										<cite>John C.</cite>
									</blockquote>
								</div><!-- .pad-10 -->
							</div><!-- .item -->
						</div><!-- .col -->
						
						<div class="col col-3 sm-col-2 xs-col-1">
							<div class="item dark-bg">
								<div class="pad-10 center testimonial">
									<blockquote>
									
										"Lorem ipsum dolor sit amet,
										consectetuer adipiscing elit. Aenean
										commodo ligula eget dolor. Aenean
										massa. Cum sociis natoque penatibus et
										magnis dis parturient monte.
										Lorem ipsum dolor sit amet,
										consectetuer adipiscing elit. Aenean
										commodo ligula eget dolor. Aenean
										massa. Cum sociis natoque penatibus et
										magnis dis parturient montes."
									
										<br>
									
										<cite>John C.</cite>
									</blockquote>
								</div><!-- .pad-10 -->
							</div><!-- .item -->
						</div><!-- .col -->
						
						<div class="col col-3 sm-col-2 xs-col-1">
							<div class="item dark-bg">
								<div class="pad-10 center testimonial">
									<blockquote>
									
										"Lorem ipsum dolor sit amet,
										consectetuer adipiscing elit. Aenean
										commodo ligula eget dolor. Aenean
										massa. Cum sociis natoque penatibus et
										magnis dis parturient montes."
									
										<br>
									
										<cite>John C.</cite>
									</blockquote>
								</div><!-- .pad-10 -->
							</div><!-- .item -->
						</div><!-- .col -->
						
						<div class="col col-3 sm-col-2 xs-col-1">
							<div class="item dark-bg">
								<div class="pad-10 center testimonial">
									<blockquote>
									
										"Lorem ipsum dolor sit amet,
										consectetuer adipiscing elit. Aenean
										commodo ligula eget dolor. Aenean
										massa. Cum sociis natoque penatibus et
										magnis dis parturient monte.
										Lorem ipsum dolor"
									
										<br>
									
										<cite>John C.</cite>
									</blockquote>
								</div><!-- .pad-10 -->
							</div><!-- .item -->
						</div><!-- .col -->
						
						<div class="col col-3 sm-col-2 xs-col-1">
							<div class="item dark-bg">
								<div class="pad-10 center testimonial">
									<blockquote>
									
										"Lorem ipsum dolor sit amet,
										consectetuer adipiscing elit. Aenean
										commodo ligula eget dolor. Aenean
										massa. Cum sociis natoque penatibus et
										magnis dis parturient monte.
										Lorem ipsum dolor sit amet,
										consectetuer adipiscing elit. Aenean
										commodo ligula eget dolor."
									
										<br>
									
										<cite>John C.</cite>
									</blockquote>
								</div><!-- .pad-10 -->
							</div><!-- .item -->
						</div><!-- .col -->
						
						
						
					</div><!-- .grid -->

				
				</div><!-- .filter-contents -->
				
			</div><!-- .filter-section -->
			
		</section>
		
	</div><!-- .body -->
</div><!-- .sw -->


<?php include('inc/i-footer.php'); ?>