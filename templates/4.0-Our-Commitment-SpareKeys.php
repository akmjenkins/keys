<?php $bodyclass = ''; ?>
<?php include('inc/i-header.php'); ?>

<div class="hero">
	<div class="hero-wrap">
	
		<div class="hero-slide" data-src="../assets/dist/images/temp/hero/hero-1.jpg">
			<div class="hero-item" style="background-image: url(../assets/dist/images/temp/hero/hero-1.jpg);"></div>
			
			<div class="hero-content d-bg">
				<h1 class="hero-title">Our Commitment</h1>
				<span class="hero-subtitle">More than just a basic service.</span>
			</div><!-- .hero-content -->
			
		</div>
		
		<div class="hero-slide" data-src="../assets/dist/images/temp/hero/hero-2.jpg">
			<div class="hero-item" style="background-image: url(../assets/dist/images/temp/hero/hero-2.jpg);"></div>
		</div>
		
		<div class="hero-slide" data-src="../assets/dist/images/temp/hero/hero-3.jpg">
			<div class="hero-item" style="background-image: url(../assets/dist/images/temp/hero/hero-3.jpg);"></div>
		</div>
		
		<div class="hero-slide" data-src="../assets/dist/images/temp/hero/hero-4.jpg">
			<div class="hero-item" style="background-image: url(../assets/dist/images/temp/hero/hero-1.jpg);"></div>
		</div>

		
	</div><!-- .hero-wrap -->
	
</div><!-- .hero -->

<div class="sw full">
	<div class="body">
		
		<section>
			<div class="article-body center">
			
				<span class="circle-graphic-wrap">
					<span class="circle-graphic lazybg" data-src="../assets/dist/images/temp/circle-graphics/call.png"></span>
				</span><!-- .circle-graphic-wrap -->
				
				<div class="section-title">
					<h2 class="title">Secure Storage</h2>
				</div><!-- .section-title -->
				
				<p>Upon receiving your keys, they will be catalogued and photographed based on your unique security code. 
				Once you have confirmed each set is yours, we will store them within tamper-proof envelopes in a fire-proof safe 
				located in our undisclosed location. Complete with an alarm system, CCTV, and 24-hour on-site security, 
				you can rest assured that your keys are safe.</p>
				  
				<p>Note: In case of an unlikely break-in, your keys will only be identifiable by the unique security code 
				they are associated with since all other info (address, first name, etc.) is stored electronically in a secure database. 
				Without access to this database, your keys are useless.</p>
				
			</div>
		</section>
		
		<hr />
		
		<section>
		
				<div class="halved-grid">
					<div class="grid">
						<div class="col col-2 sm-col-1">
							<div class="item">
							
								<div class="article-body center">
								
									<span class="circle-graphic-wrap">
										<span class="circle-graphic lazybg" data-src="../assets/dist/images/temp/circle-graphics/clock.png"></span>
									</span><!-- .circle-graphic-wrap -->
									
									<div class="section-title">
										<h2 class="title">Your Identity</h2>
									</div><!-- .section-title -->
									
									<p>In order to properly catalogue your keys, we do require a minimal amount of information to identify you 
									and associate each set of keys with your account. However, through a number of systems we have in place, 
									this information is kept simple and the only time everything is connected is when you request your keys. In fact, 
									even when your keys are requested, we still avoid determining things such as your home address by delivering 
									your keys to a pre-determined location away from where you live.</p>
									
								</div>
								
							</div>
						</div><!-- .col -->
						<div class="col col-2 sm-col-1">
							<div class="item">
							
								<div class="article-body center">
								
									<span class="circle-graphic-wrap">
										<span class="circle-graphic lazybg" data-src="../assets/dist/images/temp/circle-graphics/key.png"></span>
									</span><!-- .circle-graphic-wrap -->
									
									<div class="section-title">
										<h2 class="title">Your Keys</h2>
									</div><!-- .section-title -->
									
									<p>From the moment you mail us your keys until you request them, the only thing associated with them physically 
									is your unique security code. It is only through our database we are able to determine which keys belong to you &amp;this database 
									itself is only used for this purpose when you require your keys and contact us for them. Through a variety of security measures, 
									including a number of security questions &amp;the use of a specific passphrase you create, we will verify your identity prior to sending your keys out for delivery.</p>
									
								</div>
								
							</div>
						</div><!-- .col -->
					</div><!-- .grid -->
				</div><!-- .halved-grid -->
		
		</section>
		
		<hr />
		
		<section>
			<div class="article-body center">
				<span class="circle-graphic-wrap">
					<span class="circle-graphic lazybg" data-src="../assets/dist/images/temp/circle-graphics/comment.png"></span>
				</span><!-- .circle-graphic-wrap -->
				
				<div class="section-title">
				<h2 class="title">Credibility</h2>
				</div><!-- .section-title -->
				
			</div><!-- .article-body -->
			
			<div class="grid eqh vcenter">
				<div class="col-3 col sm-col-1">
					<div class="item dark-bg">
						<div class="pad-20 center testimonial">
							<blockquote>
							
								"Lorem ipsum dolor sit amet,
								consectetuer adipiscing elit. Aenean
								commodo ligula eget dolor. Aenean
								massa. Cum sociis natoque penatibus et
								magnis dis parturient monte.
								Lorem ipsum dolor sit amet,
								consectetuer adipiscing elit. Aenean
								commodo ligula eget dolor. Aenean
								massa. Cum sociis natoque penatibus et
								magnis dis parturient montes."
							
								<br />
							
								<cite>John C.</cite>
							</blockquote>
						</div><!-- .pad-20 -->
					</div>
				</div><!-- .col -->
				<div class="col-3 col sm-col-1">
					<div class="item dark-bg">
						<div class="pad-20 center testimonial">
						<blockquote>
						
							"Lorem ipsum dolor sit amet,
							consectetuer adipiscing elit. Aenean
							commodo ligula eget dolor. Aenean
							massa. Cum sociis natoque penatibus et
							magnis dis parturient monte.
							Lorem ipsum dolor sit amet,
							consectetuer adipiscing elit.
							<br />
							<cite>John C.</cite>
						</blockquote>
						</div><!-- .pad-20 -->
					</div>
				</div><!-- .col -->
				<div class="col-3 col sm-col-1">
					<div class="item dark-bg">
						<div class="pad-20 center testimonial">
						<blockquote>
							"Spare keys is neat."
							<br />
							<cite>John C.</cite>
						</blockquote>
						</div><!-- .pad-20 -->
					</div>
				</div><!-- .col -->
			</div><!-- .grid -->
		</section>
		
		<hr />
		
		<section>
			
			<?php include('inc/i-popular-questions.php'); ?>
			
		</section>
		
	</div><!-- .body -->
</div><!-- .sw -->


<?php include('inc/i-footer.php'); ?>