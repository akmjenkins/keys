<?php $bodyclass = ''; ?>
<?php include('inc/i-header.php'); ?>

<div class="hero">
	<div class="hero-wrap">
	
		<div class="hero-slide" data-src="../assets/dist/images/temp/hero/hero-1.jpg">
			<div class="hero-item" style="background-image: url(../assets/dist/images/temp/hero/hero-1.jpg);"></div>
			
			<div class="hero-content d-bg">
				<h1 class="hero-title">Plans &amp; Pricing</h1>
				<span class="hero-subtitle">Stop worrying.</span>
			</div><!-- .hero-content -->
			
		</div>
		
		<div class="hero-slide" data-src="../assets/dist/images/temp/hero/hero-2.jpg">
			<div class="hero-item" style="background-image: url(../assets/dist/images/temp/hero/hero-2.jpg);"></div>
		</div>
		
		<div class="hero-slide" data-src="../assets/dist/images/temp/hero/hero-3.jpg">
			<div class="hero-item" style="background-image: url(../assets/dist/images/temp/hero/hero-3.jpg);"></div>
		</div>
		
		<div class="hero-slide" data-src="../assets/dist/images/temp/hero/hero-4.jpg">
			<div class="hero-item" style="background-image: url(../assets/dist/images/temp/hero/hero-1.jpg);"></div>
		</div>

		
	</div><!-- .hero-wrap -->
	
</div><!-- .hero -->

<div class="sw full">
	<div class="body">
		
		<section>
		
				<div class="halved-grid">
					<div class="grid nopad">
						<div class="col col-2 sm-col-1">
							<div class="item">
							
								<div class="article-body center">
								
									<span class="circle-graphic-wrap">
										<span class="circle-graphic lazybg" data-src="../assets/dist/images/temp/circle-graphics/house.png"></span>
									</span><!-- .circle-graphic-wrap -->
									
									<div class="section-title">
										<h2 class="title">Residential</h2>
										<span class="subtitle">Don't get left out in the cold. Or rain. Or fog.</span>
									</div><!-- .section-title -->
									
									<p>As a homeowner in Newfoundland, being locked out of your home, car, cabin, or any other number of important things can be a serious matter. 
									Given our unpredictable weather, and the high cost of a locksmith, one simple mistake can easily snowball. Thankfully, Spare Keys has an easy 
									and cost-effective solution.</p>
									
									<br />
									
									<a href="#" class="blue button">Learn More</a>
									
								</div>
								
							</div>
						</div><!-- .col -->
						<div class="col col-2 sm-col-1">
							<div class="item">
							
								<div class="article-body center">
								
									<span class="circle-graphic-wrap">
										<span class="circle-graphic lazybg" data-src="../assets/dist/images/temp/circle-graphics/commercial.png"></span>
									</span><!-- .circle-graphic-wrap -->
									
									<div class="section-title">
										<h2 class="title">Commerical</h2>
										<span class="subtitle">Save valuable time and money.</span>
									</div><!-- .section-title -->
									
									<p>No matter how big your company is, being locked out of your business, car, or any other important part of what you do is a costly accident. 
									With Spare Keys, you can avoid this thanks to our simple set-up, quick key retrieval, and great options available to our business clients.</p>
									
									<br />
									
									<a href="#" class="blue button">Learn More</a>
									
								</div><!-- .article-body -->
								
							</div>
						</div><!-- .col -->
					</div><!-- .grid -->
				</div><!-- .halved-grid -->
				
		</section>
		
	</div><!-- .body -->
</div><!-- .sw -->


<?php include('inc/i-footer.php'); ?>