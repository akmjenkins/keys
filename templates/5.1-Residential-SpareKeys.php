<?php $bodyclass = ''; ?>
<?php include('inc/i-header.php'); ?>

<div class="hero">
	<div class="hero-wrap">
	
		<div class="hero-slide" data-src="../assets/dist/images/temp/hero/hero-1.jpg">
			<div class="hero-item" style="background-image: url(../assets/dist/images/temp/hero/hero-1.jpg);"></div>
			
			<div class="hero-content d-bg">
				<h1 class="hero-title">Residential</h1>
				<span class="hero-subtitle">Don't get left out in the cold. Or rain. Or fog.</span>
				
			</div><!-- .hero-content -->
			
		</div>
		
		<div class="hero-slide" data-src="../assets/dist/images/temp/hero/hero-2.jpg">
			<div class="hero-item" style="background-image: url(../assets/dist/images/temp/hero/hero-2.jpg);"></div>
		</div>
		
		<div class="hero-slide" data-src="../assets/dist/images/temp/hero/hero-3.jpg">
			<div class="hero-item" style="background-image: url(../assets/dist/images/temp/hero/hero-3.jpg);"></div>
		</div>
		
		<div class="hero-slide" data-src="../assets/dist/images/temp/hero/hero-4.jpg">
			<div class="hero-item" style="background-image: url(../assets/dist/images/temp/hero/hero-1.jpg);"></div>
		</div>

		
	</div><!-- .hero-wrap -->
	
</div><!-- .hero -->

<div class="sw full">
	<div class="body">
		
		<section>
		
			<div class="main-body">
				<div class="content">
					<div class="article-body">					
						<p class="excerpt">
							As a homeowner in Newfoundland, being locked out of your home, car, cabin, or any other number of important things can be a serious matter. 
							Given our unpredictables as a homeowner in Newfoundland, being locked out of your home, car, cabin, or any other number of important things 
							can be a serious matter. Given our unpredictable weather, and the high cost of a locksmith, one simple mistake can easily snowball. 
							Thankfully, Spare Keys has an easy and cost-effective solution.
						</p>
						
						<p>
							Our Residential plan is a great way to ensure you never have to stand outside your own home unable to get in again. Plus, thanks to our 
							quick retrieval service, you’ll no longer find yourself out in the rain or snow waiting hours for a locksmith to charge you due to an 
							unfortunate event.
						</p>
						
						<p>
							Along with the comfort of knowing you’ll always have easy access to a spare set of your house keys, we can also store keys for a 
							number of other important things in your life. So, when you find yourself at the grocery store with your ice cream melting and 
							your keys sitting inside your car, one simple call can lead to a solution. And maybe sundaes that night!le weather, and the high 
							cost of a locksmith, one simple mistake can easily snowball. Thankfully, Spare Keys has an easy and cost-effective solution.
						</p>
					</div><!-- .article-body -->
				</div><!-- .content -->
				<aside class="sidebar">
					<div class="pricing-panel">
					
						<div class="pricing-panel-title dark-bg">
							<span class="h3-style">Residential</span>
						</div><!-- .pricing-panel-title -->
						
						<span class="pricing-panel-price">
							<span>&nbsp;</span>
							$3.99 <small>per month</small>
						</span>
						
						<ul>
							<li>Store up to 6 keys</li>
							<li>Available 24/7</li>
							<li>More secure than hiding a key.</li>
							<li>Very affordable</li>
						</ul>
						
						<div class="pricing-panel-btns">
							<a href="#" class="button">Learn More</a>
							<a href="#" class="button blue">Purchase Now</a>
						</div>
						
					</div><!-- .pricing-panel -->
				</aside>
			</div><!-- .main-body -->
		
		</section>
		
	</div><!-- .body -->
</div><!-- .sw -->


<?php include('inc/i-footer.php'); ?>