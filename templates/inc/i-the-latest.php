			<div class="section-title">
				<h2 class="title">The Latest</h2>
				<span class="subtitle">Lorem Ipsum Dolor</span>
			</div><!-- .section-title -->
			
			<div class="grid fill eqh latest-grid">
				<div class="col col-3 sm-col-2 xs-col-1">
					<div class="item">
						<div class="pad-20">
							
							<time datetime="2014-05-24" class="t">
								<span class="t-day">24</span>
								<span class="t-month-year">
									<span class="t-month">May</span>
									<span class="t-year">2014</span>
								</span>
							</time>
							
							<div class="hgroup">
								<span class="h4-style">Latest News Title</span>
								<span class="subtitle">Subtitle Here</span>
							</div><!-- .hgroup -->
							
							<p>
								Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. 
								Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. 
								Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem.
							</p>
							
							<div class="latest-button">
								<a href="#" class="button">View All</a>
							</div><!-- .latest-button -->
							
						</div>
					</div><!-- .item -->
				</div><!-- .col -->
				<div class="col col-3 sm-col-2 xs-col-1">
					<div class="item">
						<div class="pad-20">
							
							<time datetime="2014-05-24" class="t">
								<span class="t-day">24</span>
								<span class="t-month-year">
									<span class="t-month">May</span>
									<span class="t-year">2014</span>
								</span>
							</time>
							
							<div class="hgroup">
								<span class="h4-style">Upcoming Event Here</span>
								<span class="subtitle">Subtitle Here</span>
							</div><!-- .hgroup -->
							
							<p>
								Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque.
							</p>
							
							<div class="event-meta">
								<div class="event-meta-row">
									<span class="event-meta-label">Date</span>
									May 26, 2014
								</div><!-- .event-meta-row -->
								<div class="event-meta-row">
									<span class="event-meta-label">Time</span>
									3:00 - 5:00pm
								</div><!-- .event-meta-row -->
								<div class="event-meta-row">
									<span class="event-meta-label">Place</span>
									123 Address Street
								</div><!-- .event-meta-row -->
							</div><!-- .event-meta -->
							
							<div class="latest-button">
								<a href="#" class="button">View All</a>
							</div><!-- .latest-button -->
							
						</div>
					</div><!-- .item -->
				</div><!-- .col -->
				<div class="col col-3 sm-col-1 xs-col-1">
					<div class="item dark-bg testimonial">
						<div class="pad-20">
							<blockquote>
								"Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient monte.
								Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes."
								
								<br />
								
								<cite>Customer Testimonial</cite>
							</blockquote>
							
						</div><!-- .pad-20 -->
						
						<div class="latest-button">
							<a href="#" class="button">View All</a>
						</div><!-- .latest-button -->
					</div><!-- .item -->
				</div><!-- .col -->
			</div><!-- .grid -->