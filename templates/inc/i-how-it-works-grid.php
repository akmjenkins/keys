			<div class="section-title">
				<h2 class="title">How It Works</h2>
				<span class="subtitle">Lorem Ipsum Dolor</span>
			</div><!-- .section-title -->
			
			<div class="tab-wrapper how-it-works">
			
				<div class="tab-controls">
					<span class="tab-control selected">Sending Your Keys</span>
					<span class="tab-control">Receiving Your Keys</span>
					
					<div class="selector with-arrow">
						<select class="tab-controller">
							<option>Sending Your Keys</option>
							<option>Receiving Your Keys</option>
						</select>
						<span class="value">&nbsp;</span>
					</div>
					
				</div><!-- .tab-controls -->
				
				<div class="tab-holder">
				
					<div class="tab selected">
						<div class="pad-20 sm-pad-0">
							<div class="grid">
							
								<div class="col col-3 sm-col-1">
									<div class="item">
										<span class="circle-graphic-wrap">
											<span class="circle-graphic lazybg" data-src="../assets/dist/images/temp/circle-graphics/send-keys.png"></span>
										</span><!-- .circle-graphic-wrap -->									
										
										<span class="how-it-works-text">Tag and Send us Your Spare Keys.</span>
									</div>
								</div><!-- .col -->
								
								<div class="col col-3 sm-col-1">
									<div class="item">
										<span class="circle-graphic-wrap">
											<span class="circle-graphic lazybg" data-src="../assets/dist/images/temp/circle-graphics/photograph.png"></span>
										</span><!-- .circle-graphic-wrap -->
										
										<span class="how-it-works-text">We photograph them and send you the pictures</span>
									</div>
								</div><!-- .col -->
								
								<div class="col col-3 sm-col-1">
									<div class="item">
										<span class="circle-graphic-wrap">
											<span class="circle-graphic lazybg" data-src="../assets/dist/images/temp/circle-graphics/stored-securely.png"></span>
										</span><!-- .circle-graphic-wrap -->
										
										<span class="how-it-works-text">Once confirmed your keys are stored securely</span>
									</div>
								</div><!-- .col -->
								
							</div><!-- .grid -->
						</div><!-- .pad-20 -->
					</div><!-- .tab -->
					
					<div class="tab">
					
						<div class="pad-20 sm-pad-0">
							<div class="grid">
							
								<div class="col col-3 sm-col-1">
									<div class="item">
										<span class="circle-graphic-wrap">
											<span class="circle-graphic lazybg" data-src="../assets/dist/images/temp/circle-graphics/call.png"></span>
										</span><!-- .circle-graphic-wrap -->
										
										<span class="how-it-works-text">Call us to request Your keys.</span>
									</div>
								</div><!-- .col -->
								
								<div class="col col-3 sm-col-1">
									<div class="item">
										<span class="circle-graphic-wrap">
											<span class="circle-graphic lazybg" data-src="../assets/dist/images/temp/circle-graphics/question.png"></span>
										</span><!-- .circle-graphic-wrap -->
										
										<span class="how-it-works-text">Answer the security questions we ask.</span>
									</div>
								</div><!-- .col -->
								
								<div class="col col-3 sm-col-1">
									<div class="item">
										<span class="circle-graphic-wrap">
											<span class="circle-graphic lazybg" data-src="../assets/dist/images/temp/circle-graphics/delivery.png"></span>
										</span><!-- .circle-graphic-wrap -->
										
										<span class="how-it-works-text">Your keys will be delivered to you within the hour.</span>
									</div>
								</div><!-- .col -->
								
							</div><!-- .grid -->
						</div><!-- .pad-20 -->
					
					</div><!-- .tab -->
					
				</div><!-- .tab-holder -->
				
			</div><!-- .tab-wrapper -->