			<footer class="d-bg">
				<div class="sw">
				
					<div class="footer-blocks">
						<div class="footer-block">
						
							<a href="#" class="footer-logo">
								<img src="../assets/dist/images/spare-keys-logo.svg" alt="Spare Keys Logo">
							</a>
						
							<div class="footer-contact">
								<h6>Connect</h6>
								
								<address>
									Address Line 1 <br />
									Address Line 2
								</address>
								
								<span class="block">Telephone: 709.555.5555</span>
								<span class="block">Inquiries: <a href="#">info@sparekeys.ca</a></span>
								
							</div><!-- .footer-contact -->
						
						</div><!-- .footer-block -->
						
						<div class="footer-block">
						
							<h6>Subscribe</h6>
							<span class="block">Enter your e-mail address below:</span>
							
							<form action="/" method="post" class="single-form">
								<fieldset>
									<input type="email" name="email" placeholder="Subscribe to Spare Keys Updates...">
									<button class="t-fa-abs fa-chevron-right">Subscribe</button>
								</fieldset>
							</form>
						
						</div><!-- .footer-block -->
						
					</div><!-- .footer-blocks -->
				
				</div><!-- .sw -->
				
				<div class="copyright">
					<div class="sw">							
						<ul>
							<li>Copyright &copy; <?php echo date('Y'); ?> <a href="/">Spare Keys</a></li>
							<li><a href="#">Legal</a></li>
							<li><a href="#">Sitemap</a></li>
						</ul>
						
						<a href="http://jac.co" rel="external" title="JAC. We Create." id="jac"><img src="../assets/dist/images/jac-logo.svg" alt="JAC Logo."></a>
					</div><!-- .sw -->
				</div><!-- .copyright -->
			</footer><!-- .footer -->
		
		</div><!-- .page-wrapper -->
			
		<script>
			var templateJS = {
				templateURL: 'http://dev.me/keys',
				CACHE_BUSTER: '<?php echo time(); ?>'	/* cache buster - set this to some unique string whenever an update is performed on js/css files, or when an admin is logged in */
			};
		</script>
		
		<script src="../assets/dist/js/main.js"></script>
	</body>
</html>