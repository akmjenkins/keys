			<div class="section-title">
				<h2 class="title">Popular Questions</h2>
				<span class="subtitle">Find Your Answers</span>
			</div><!-- .section-title -->
			
			<div class="acc with-indicators">
			
				<div class="acc-item">
				
					<div class="acc-item-handle">
						What information do you require for me to set up an account?
					</div><!-- .acc-item-handle -->
					
					<div class="acc-item-content">
						<div class="article-body">
							<div class="pad-20 sm-pad-5">
								<p>
									To ensure privacy and security, Spare Keys will require the following information
								</p>
								
								<ul class="chev">
									<li>Your first name</li>
									<li>Email address</li>
									<li>Mobile phone number</li>
									<li>Two personal information questions, chosen by you from a number of questions. </li>
									<li>A personal passphrase</li>
								</ul>
							</div><!-- .pad-20 -->
						</div><!-- .article-body -->
					</div><!-- .acc-item-content -->
					
				</div><!-- .acc-item -->
				
				<div class="acc-item">
				
					<div class="acc-item-handle">
						How do I send my keys?
					</div><!-- .acc-item-handle -->
					
					<div class="acc-item-content">
						<div class="article-body">
							<div class="pad-20 sm-pad-5">
								<p>
									To ensure privacy and security, Spare Keys will require the following information
								</p>
								
								<ul class="chev">
									<li>Your first name</li>
									<li>Email address</li>
									<li>Mobile phone number</li>
									<li>Two personal information questions, chosen by you from a number of questions. </li>
									<li>A personal passphrase</li>
								</ul>
							</div><!-- .pad-20 -->
						</div><!-- .article-body -->
					</div><!-- .acc-item-content -->
					
				</div><!-- .acc-item -->
				
				<div class="acc-item">
				
					<div class="acc-item-handle">
						How will my keys be stored?
					</div><!-- .acc-item-handle -->
					
					<div class="acc-item-content">
						<div class="article-body">
							<div class="pad-20 sm-pad-5">
								<p>
									To ensure privacy and security, Spare Keys will require the following information
								</p>
								
								<ul class="chev">
									<li>Your first name</li>
									<li>Email address</li>
									<li>Mobile phone number</li>
									<li>Two personal information questions, chosen by you from a number of questions. </li>
									<li>A personal passphrase</li>
								</ul>
							</div><!-- .pad-20 -->
						</div><!-- .article-body -->
					</div><!-- .acc-item-content -->
					
				</div><!-- .acc-item -->
				
			</div><!-- .acc -->
