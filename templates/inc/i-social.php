<div class="social">
	<a href="#" title="Like Spare Keys on Facebook" class="social-fb" rel="external">Like Spare Keys on Facebook</a>
	<a href="#" title="Follow Spare Keys on Twitter" class="social-tw" rel="external">Follow Spare Keys on Twitter</a>
	<a href="#" title="Connect with Spare Keys on LinkedIn" class="social-in" rel="external">Connect with Spare Keys on LinkedIn</a>
	<a href="#" title="Stay up to date with Spare Keys via RSS" class="social-rss" rel="external">Stay up to date with Spare Keys via RSS</a>
</div><!-- .social -->