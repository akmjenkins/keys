<div class="hero">
	<div class="hero-wrap">
	
		<div class="hero-slide" data-src="../assets/dist/images/temp/hero/hero-1.jpg">
			<div class="hero-item" style="background-image: url(../assets/dist/images/temp/hero/hero-1.jpg);"></div>
			
			<div class="hero-content d-bg">
				<span class="hero-title">
					Image + Caption Here About <strong>Security</strong>
				</span><!-- .hero-title -->
				
				<a href="#" class="hero-button">Learn More</a>
				
			</div><!-- .hero-content -->
			
		</div>
		
		<div class="hero-slide" data-src="../assets/dist/images/temp/hero/hero-2.jpg">
			<div class="hero-item" style="background-image: url(../assets/dist/images/temp/hero/hero-2.jpg);"></div>
			<div class="hero-content d-bg">
				
				<span class="hero-title">
					Image + Caption Here About <strong>Reliability</strong>
				</span><!-- .hero-title -->
				
				<a href="#" class="hero-button">Learn More</a>
				
			</div><!-- .hero-content -->
			
		</div>
		
		<div class="hero-slide" data-src="../assets/dist/images/temp/hero/hero-3.jpg">
			<div class="hero-item" style="background-image: url(../assets/dist/images/temp/hero/hero-3.jpg);"></div>
			<div class="hero-content d-bg">
				
				<span class="hero-title">
					Image + Caption Here About <strong>Credibility</strong>
				</span><!-- .hero-title -->
				
				<a href="#" class="hero-button">Learn More</a>
				
			</div><!-- .hero-content -->
			
		</div>
		
		<div class="hero-slide" data-src="../assets/dist/images/temp/hero/hero-4.jpg">
			<div class="hero-item" style="background-image: url(../assets/dist/images/temp/hero/hero-1.jpg);"></div>
			
			<div class="hero-content d-bg">
				<span class="hero-title">
					Image + Caption Here About <strong>Affordability</strong>
				</span><!-- .hero-title -->
				
				<a href="#" class="hero-button">Learn More</a>
				
			</div><!-- .hero-content -->
			
		</div>

		
	</div><!-- .hero-wrap -->
	
	<div class="hero-nav">
		<div class="sw">		
		
			<div class="hero-control-wrap">
				<div class="btn selected">
					Security
					<i class="fa fa-lock"></i>
				</div><!-- .btn -->
				
				<div class="btn">
					Reliability
					<i class="fa fa-check-circle"></i>
				</div><!-- .btn -->
				
				<div class="btn">
					Credibility
					<i class="fa fa-briefcase"></i>
				</div><!-- .btn -->
				
				<div class="btn">
					Affordability
					<i class="fa fa-dollar"></i>
				</div><!-- .btn -->
			</div><!-- .home-hero-control-wrap -->
			
		</div><!-- .sw -->
	</div><!-- .home-hero-nav -->
	
</div><!-- .hero -->