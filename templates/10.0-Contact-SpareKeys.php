<?php $bodyclass = ''; ?>
<?php include('inc/i-header.php'); ?>

<div class="hero">
	<div class="hero-wrap">
	
		<div class="hero-slide" data-src="../assets/dist/images/temp/hero/hero-1.jpg">
			<div class="hero-item" style="background-image: url(../assets/dist/images/temp/hero/hero-1.jpg);"></div>
			
			<div class="hero-content d-bg">
				<h1 class="hero-title">Contact</h1>
				<span class="hero-subtitle">Get us to work for you.</span>
				
			</div><!-- .hero-content -->
			
		</div>
		
		<div class="hero-slide" data-src="../assets/dist/images/temp/hero/hero-2.jpg">
			<div class="hero-item" style="background-image: url(../assets/dist/images/temp/hero/hero-2.jpg);"></div>
		</div>
		
		<div class="hero-slide" data-src="../assets/dist/images/temp/hero/hero-3.jpg">
			<div class="hero-item" style="background-image: url(../assets/dist/images/temp/hero/hero-3.jpg);"></div>
		</div>
		
		<div class="hero-slide" data-src="../assets/dist/images/temp/hero/hero-4.jpg">
			<div class="hero-item" style="background-image: url(../assets/dist/images/temp/hero/hero-1.jpg);"></div>
		</div>

		
	</div><!-- .hero-wrap -->
	
</div><!-- .hero -->

<div class="sw full">
	<div class="body">
		
		<section>
		
			<blockquote class="center">
				Contact us now so we can be there for you when you need a spare key at anytime, day or night.
			</blockquote>
			
		</section>
		
		<section>
		
			<div class="grid">
				<div class="col col-2 sm-col-1">
					<div class="item">
					
						<form action="/" method="post" class="body-form">
							<fieldset>
								
								<input type="text" name="name" placeholder="Name">
								<input type="email" name="email" placeholder="E-mail Address">
								<input type="tel" name="phone" placeholder="Phone">
								<textarea name="message" placeholder="Message"></textarea>
								<button class="button" type="submit">Submit</button>
								
								
							</fieldset>
						</form><!-- .body-form -->
					
					</div><!-- .item -->
				</div><!-- .col -->
				<div class="col-2 col sm-col-1">
				
					<div class="item center">
						<div class="pad-20">
							
							<div class="ar" data-ar="40">
								<iframe class="ar-child" frameborder="0" style="border:0" src="https://www.google.com/maps/embed/v1/place?key=<?php echo $GOOGLE_MAPS_API_KEY; ?>&q=Mount Pearl, NL"></iframe>
							</div>
							
							<br />
							
							<address>
								PO Box 3055 Pealgate Post Office <br />
								Mount Pearl, NL
							</address>
							
							<br />
							
							<span class="block">T: 1-709-764-8888</span>
							
						</div><!-- .pad-20 -->
					</div><!-- .item -->
					
				</div><!-- .col -->
			</div><!-- .grid -->
			
			
		</section>
		
	</div><!-- .body -->
</div><!-- .sw -->


<?php include('inc/i-footer.php'); ?>