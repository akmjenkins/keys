<?php $bodyclass = ''; ?>
<?php include('inc/i-header.php'); ?>

<div class="hero">
	<div class="hero-wrap">
	
		<div class="hero-slide" data-src="../assets/dist/images/temp/hero/hero-1.jpg">
			<div class="hero-item" style="background-image: url(../assets/dist/images/temp/hero/hero-1.jpg);"></div>
			
			<div class="hero-content d-bg">
				<h1 class="hero-title">This is the headline</h1>
				<span class="hero-subtitle">This is the subheadline</span>
				
			</div><!-- .hero-content -->
			
		</div>
		
		<div class="hero-slide" data-src="../assets/dist/images/temp/hero/hero-2.jpg">
			<div class="hero-item" style="background-image: url(../assets/dist/images/temp/hero/hero-2.jpg);"></div>
		</div>
		
		<div class="hero-slide" data-src="../assets/dist/images/temp/hero/hero-3.jpg">
			<div class="hero-item" style="background-image: url(../assets/dist/images/temp/hero/hero-3.jpg);"></div>
		</div>
		
		<div class="hero-slide" data-src="../assets/dist/images/temp/hero/hero-4.jpg">
			<div class="hero-item" style="background-image: url(../assets/dist/images/temp/hero/hero-1.jpg);"></div>
		</div>

		
	</div><!-- .hero-wrap -->
	
</div><!-- .hero -->

<div class="sw full">
	<div class="body">
		
		<section>
		
			<blockquote class="center">
				Find out what’s happening with Spare Keys and more as we post news items related to our services and other things.
			</blockquote>
		
			<div class="main-body">
				<div class="content">
					<div class="article-body">
					
						<div class="article-meta">
							<time datetime="2014-05-24" class="t" pubdate>
								<span class="t-day">24</span>
								<span class="t-month-year">
									<span class="t-month">May</span>
									<span class="t-year">2014</span>
								</span>
							</time>
							
							<div class="event-meta">
								<div class="event-meta-row">
									<span class="event-meta-label">Time</span>
									3:00 - 5:00pm
								</div><!-- .event-meta-row -->
								<div class="event-meta-row">
									<span class="event-meta-label">Place</span>
									123 Address Street
								</div><!-- .event-meta-row -->
							</div><!-- .event-meta -->
							
						</div><!-- .article-meta -->
						
						<p class="excerpt">
							As a homeowner in Newfoundland, being locked out of your home, car, cabin, or any other number of important things can be a serious matter. 
							Given our unpredictables as a homeowner in Newfoundland, being locked out of your home, car, cabin, or any other number of important things 
							can be a serious matter. Given our unpredictable weather, and the high cost of a locksmith, one simple mistake can easily snowball. 
							Thankfully, Spare Keys has an easy and cost-effective solution.
						</p>
						
						<p>
							Our Residential plan is a great way to ensure you never have to stand outside your own home unable to get in again. Plus, thanks to our quick 
							retrieval service, you’ll no longer find yourself out in the rain or snow waiting hours for a locksmith to charge you due to an unfortunate event.
						</p>
 
						<p>
							Along with the comfort of knowing you’ll always have easy access to a spare set of your house keys, we can also store keys for a number of other 
							important things in your life. So, when you find yourself at the grocery store with your ice cream melting.
						</p>
						
					</div><!-- .article-body -->
				</div><!-- .content -->
				<aside class="sidebar">
					
					<div class="bordered-item pad-20 mod-archives">
						<h4>Archives</h4>
						
						<ul class="styled">
							<li><a href="#l1" class="inline">Lorem Ipsum dolor</a></li>
							<li><a href="#l2" class="inline">Lorem Ipsum dolor</a></li>
							<li><a href="#l3" class="inline">Lorem Ipsum dolor</a></li>
							<li><a href="#l4" class="inline">Lorem Ipsum dolor</a></li>
							<li><a href="#l5" class="inline">Lorem Ipsum dolor</a></li>
						</ul>
						
					</div><!-- .bordered-item -->
					
				</aside>
			</div><!-- .main-body -->
		
		</section>
		
	</div><!-- .body -->
</div><!-- .sw -->


<?php include('inc/i-footer.php'); ?>