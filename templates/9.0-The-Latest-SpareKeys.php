<?php $bodyclass = ''; ?>
<?php include('inc/i-header.php'); ?>

<div class="hero">
	<div class="hero-wrap">
	
		<div class="hero-slide" data-src="../assets/dist/images/temp/hero/hero-1.jpg">
			<div class="hero-item" style="background-image: url(../assets/dist/images/temp/hero/hero-1.jpg);"></div>
			
			<div class="hero-content d-bg">
				<h1 class="hero-title">The Latest</h1>
				<span class="hero-subtitle">Keep up to date</span>
				
			</div><!-- .hero-content -->
			
		</div>
		
		<div class="hero-slide" data-src="../assets/dist/images/temp/hero/hero-2.jpg">
			<div class="hero-item" style="background-image: url(../assets/dist/images/temp/hero/hero-2.jpg);"></div>
		</div>
		
		<div class="hero-slide" data-src="../assets/dist/images/temp/hero/hero-3.jpg">
			<div class="hero-item" style="background-image: url(../assets/dist/images/temp/hero/hero-3.jpg);"></div>
		</div>
		
		<div class="hero-slide" data-src="../assets/dist/images/temp/hero/hero-4.jpg">
			<div class="hero-item" style="background-image: url(../assets/dist/images/temp/hero/hero-1.jpg);"></div>
		</div>

		
	</div><!-- .hero-wrap -->
	
</div><!-- .hero -->

<div class="sw full">
	<div class="body">
		
		<section>
		
			<blockquote class="center">
				Stay current with what’s happening with Spare Keys and other related things through our news, upcoming events, and client testimonials.
			</blockquote>

		</section>
			
		<section>	
		
			<div class="section-title">
				<h2 class="title">Latest News</h2>
			</div><!-- .section-title -->
			
			<div class="grid eqh latest-grid">
				<div class="col col-3 sm-col-1">
					<div class="item">
						<div class="pad-20">
							
							<time datetime="2014-05-24" class="t">
								<span class="t-day">24</span>
								<span class="t-month-year">
									<span class="t-month">May</span>
									<span class="t-year">2014</span>
								</span>
							</time>
							
							<div class="hgroup">
								<span class="h4-style">Latest News Title Here</span>
								<span class="subtitle">Subtitle Here</span>
							</div><!-- .hgroup -->
							
							<p>
								Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. 
								Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. 
								Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem.
							</p>
							
							<div class="latest-button">
								<a href="#" class="button">Read More</a>
							</div><!-- .latest-button -->
							
						</div>
					</div><!-- .item -->
				</div><!-- .col -->
				<div class="col col-3 sm-col-1">
					<div class="item">
						<div class="pad-20">
							
							<time datetime="2014-05-24" class="t">
								<span class="t-day">24</span>
								<span class="t-month-year">
									<span class="t-month">May</span>
									<span class="t-year">2014</span>
								</span>
							</time>
							
							<div class="hgroup">
								<span class="h4-style">Latest News Title Here</span>
								<span class="subtitle">Subtitle Here</span>
							</div><!-- .hgroup -->
							
							<p>
								Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. 
								Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. 
								Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem.
							</p>
							
							<div class="latest-button">
								<a href="#" class="button">Read More</a>
							</div><!-- .latest-button -->
							
						</div>
					</div><!-- .item -->
				</div><!-- .col -->
				<div class="col col-3 sm-col-1">
					<div class="item">
						<div class="pad-20">
							
							<time datetime="2014-05-24" class="t">
								<span class="t-day">24</span>
								<span class="t-month-year">
									<span class="t-month">May</span>
									<span class="t-year">2014</span>
								</span>
							</time>
							
							<div class="hgroup">
								<span class="h4-style">Latest News Title Here</span>
								<span class="subtitle">Subtitle Here</span>
							</div><!-- .hgroup -->
							
							<p>
								Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. 
								Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. 
								Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem.
							</p>
							
							<div class="latest-button">
								<a href="#" class="button">Read More</a>
							</div><!-- .latest-button -->
							
						</div>
					</div><!-- .item -->
				</div><!-- .col -->			
			</div><!-- .grid -->
			
			
			<div class="center">
				<a href="#" class="blue button">More News</a>
			</div><!-- .center -->
		
		</section>
		
		<hr />
		
		<section>
		
			<div class="section-title">
				<h2 class="title">Latest Events</h2>
			</div><!-- .section-title -->
		
			<div class="grid eqh latest-grid">
				<div class="col col-3 sm-col-1">
					<div class="item">
						<div class="pad-20 sm-pad-10">
							
							<time datetime="2014-05-24" class="t">
								<span class="t-day">24</span>
								<span class="t-month-year">
									<span class="t-month">May</span>
									<span class="t-year">2014</span>
								</span>
							</time>
							
							<div class="hgroup">
								<span class="h4-style">Upcoming Event Here</span>
								<span class="subtitle">Subtitle Here</span>
							</div><!-- .hgroup -->
							
							<p>
								Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque.
							</p>
							
							<div class="event-meta">
								<div class="event-meta-row">
									<span class="event-meta-label">Date</span>
									May 26, 2014
								</div><!-- .event-meta-row -->
								<div class="event-meta-row">
									<span class="event-meta-label">Time</span>
									3:00 - 5:00pm
								</div><!-- .event-meta-row -->
								<div class="event-meta-row">
									<span class="event-meta-label">Place</span>
									123 Address Street
								</div><!-- .event-meta-row -->
							</div><!-- .event-meta -->
							
							<div class="latest-button">
								<a href="#" class="button">Read More</a>
							</div><!-- .latest-button -->
							
						</div>
					</div><!-- .item -->
				</div><!-- .col -->
				<div class="col col-3 sm-col-1">
					<div class="item">
						<div class="pad-20 sm-pad-10">
							
							<time datetime="2014-05-24" class="t">
								<span class="t-day">24</span>
								<span class="t-month-year">
									<span class="t-month">May</span>
									<span class="t-year">2014</span>
								</span>
							</time>
							
							<div class="hgroup">
								<span class="h4-style">Upcoming Event Here</span>
								<span class="subtitle">Subtitle Here</span>
							</div><!-- .hgroup -->
							
							<p>
								Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque.
							</p>
							
							<div class="event-meta">
								<div class="event-meta-row">
									<span class="event-meta-label">Date</span>
									May 26, 2014
								</div><!-- .event-meta-row -->
								<div class="event-meta-row">
									<span class="event-meta-label">Time</span>
									3:00 - 5:00pm
								</div><!-- .event-meta-row -->
								<div class="event-meta-row">
									<span class="event-meta-label">Place</span>
									123 Address Street
								</div><!-- .event-meta-row -->
							</div><!-- .event-meta -->
							
							<div class="latest-button">
								<a href="#" class="button">Read More</a>
							</div><!-- .latest-button -->
							
						</div>
					</div><!-- .item -->
				</div><!-- .col -->
				<div class="col col-3 sm-col-1">
					<div class="item">
						<div class="pad-20 sm-pad-10">
							
							<time datetime="2014-05-24" class="t">
								<span class="t-day">24</span>
								<span class="t-month-year">
									<span class="t-month">May</span>
									<span class="t-year">2014</span>
								</span>
							</time>
							
							<div class="hgroup">
								<span class="h4-style">Upcoming Event Here</span>
								<span class="subtitle">Subtitle Here</span>
							</div><!-- .hgroup -->
							
							<p>
								Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque.
							</p>
							
							<div class="event-meta">
								<div class="event-meta-row">
									<span class="event-meta-label">Date</span>
									May 26, 2014
								</div><!-- .event-meta-row -->
								<div class="event-meta-row">
									<span class="event-meta-label">Time</span>
									3:00 - 5:00pm
								</div><!-- .event-meta-row -->
								<div class="event-meta-row">
									<span class="event-meta-label">Place</span>
									123 Address Street
								</div><!-- .event-meta-row -->
							</div><!-- .event-meta -->
							
							<div class="latest-button">
								<a href="#" class="button">Read More</a>
							</div><!-- .latest-button -->
							
						</div>
					</div><!-- .item -->
				</div><!-- .col -->			
			</div><!-- .grid -->
			
			<div class="center">
				<a href="#" class="blue button">More Events</a>
			</div><!-- .center -->
			
		</section>
		
	</div><!-- .body -->
</div><!-- .sw -->


<?php include('inc/i-footer.php'); ?>