<?php $bodyclass = ''; ?>
<?php include('inc/i-header.php'); ?>

<div class="hero">
	<div class="hero-wrap">
	
		<div class="hero-slide" data-src="../assets/dist/images/temp/hero/hero-1.jpg">
			<div class="hero-item" style="background-image: url(../assets/dist/images/temp/hero/hero-1.jpg);"></div>
			
			<div class="hero-content d-bg">
				<h1 class="hero-title">Support</h1>
				<span class="hero-subtitle">Get help with your issue</span>
				
			</div><!-- .hero-content -->
			
		</div>
		
		<div class="hero-slide" data-src="../assets/dist/images/temp/hero/hero-2.jpg">
			<div class="hero-item" style="background-image: url(../assets/dist/images/temp/hero/hero-2.jpg);"></div>
		</div>
		
		<div class="hero-slide" data-src="../assets/dist/images/temp/hero/hero-3.jpg">
			<div class="hero-item" style="background-image: url(../assets/dist/images/temp/hero/hero-3.jpg);"></div>
		</div>
		
		<div class="hero-slide" data-src="../assets/dist/images/temp/hero/hero-4.jpg">
			<div class="hero-item" style="background-image: url(../assets/dist/images/temp/hero/hero-1.jpg);"></div>
		</div>

		
	</div><!-- .hero-wrap -->
	
</div><!-- .hero -->

<div class="sw full">
	<div class="body">
		
		<section>
			<blockquote class="center">
				In case you have an issue with our service, or something related to the loss of your keys, we are here to help.
			</blockquote>
			
			<div class="grid">
				<div class="col-2-3 col sm-col-1">
					<div class="item">
					
						<div class="acc with-indicators">
						
							<div class="acc-item">
							
								<div class="acc-item-handle">
									How can I request my keys when I need them?
								</div><!-- .acc-item-handle -->
								
								<div class="acc-item-content">
									<div class="article-body">
										<div class="pad-20 sm-pad-5">
											<p>
												Since we understand that you may need your keys at any time, day or night, we will have a service representative 
												available to answer your requests 24 hours a day, seven days a week. Depending on your preference, 
												you may contact us via phone, email, or text.
											</p>
										</div><!-- .pad-20 -->
									</div><!-- .article-body -->
								</div><!-- .acc-item-content -->
								
							</div><!-- .acc-item -->
							
							<div class="acc-item">
							
								<div class="acc-item-handle">
									How do I send you my keys?
								</div><!-- .acc-item-handle -->
								
								<div class="acc-item-content">
									<div class="article-body">
										<div class="pad-20 sm-pad-5">
											<p>
												Since we understand that you may need your keys at any time, day or night, we will have a service representative 
												available to answer your requests 24 hours a day, seven days a week. Depending on your preference, 
												you may contact us via phone, email, or text.
											</p>
										</div><!-- .pad-20 -->
									</div><!-- .article-body -->
								</div><!-- .acc-item-content -->
								
							</div><!-- .acc-item -->
							
							<div class="acc-item">
							
								<div class="acc-item-handle">
									How can I cancel my service?
								</div><!-- .acc-item-handle -->
								
								<div class="acc-item-content">
									<div class="article-body">
										<div class="pad-20 sm-pad-5">
											<p>
												Since we understand that you may need your keys at any time, day or night, we will have a service representative 
												available to answer your requests 24 hours a day, seven days a week. Depending on your preference, 
												you may contact us via phone, email, or text.
											</p>
										</div><!-- .pad-20 -->
									</div><!-- .article-body -->
								</div><!-- .acc-item-content -->
								
							</div><!-- .acc-item -->
							
						</div><!-- .acc -->
					
					</div>
				</div>
				<div class="col-1-3 col sm-col-1">
					<div class="item">
						
						<div class="contact-callout dark-bg">
							
							<span>Contact Us Now</span>
							<span>1-709-764-8888</span>
							
						</div><!-- .contact-callout -->
						
					</div>
				</div>
			</div><!-- .grid -->
			
		</section>
		
	</div><!-- .body -->
</div><!-- .sw -->


<?php include('inc/i-footer.php'); ?>