<?php $bodyclass = ''; ?>
<?php include('inc/i-header.php'); ?>

<div class="hero">
	<div class="hero-wrap">
	
		<div class="hero-slide" data-src="../assets/dist/images/temp/hero/hero-1.jpg">
			<div class="hero-item" style="background-image: url(../assets/dist/images/temp/hero/hero-1.jpg);"></div>
			
			<div class="hero-content d-bg">
				<h1 class="hero-title">Media</h1>
				<span class="hero-subtitle">For the press.</span>
				
			</div><!-- .hero-content -->
			
		</div>
		
		<div class="hero-slide" data-src="../assets/dist/images/temp/hero/hero-2.jpg">
			<div class="hero-item" style="background-image: url(../assets/dist/images/temp/hero/hero-2.jpg);"></div>
		</div>
		
		<div class="hero-slide" data-src="../assets/dist/images/temp/hero/hero-3.jpg">
			<div class="hero-item" style="background-image: url(../assets/dist/images/temp/hero/hero-3.jpg);"></div>
		</div>
		
		<div class="hero-slide" data-src="../assets/dist/images/temp/hero/hero-4.jpg">
			<div class="hero-item" style="background-image: url(../assets/dist/images/temp/hero/hero-1.jpg);"></div>
		</div>

		
	</div><!-- .hero-wrap -->
	
</div><!-- .hero -->

<div class="sw full">
	<div class="body">
		
		<section>
		
			<blockquote class="center">
				If you require anything else related to Spare Keys or the people behind the company, please contact us for more information.
			</blockquote>
			
		</section>
		
		<section>
			
			<div class="section-title">
				<h2 class="title">Company Logo</h2>
			</div><!-- .section-title -->
			
			<div class="grid">
				<div class="col col-4 sm-col-2">
					<div class="item">
					
							<div class="brand-block">
							
								<div class="bordered-item">
									<div class="pad-20">
										<img src="../assets/dist/images/spare-keys-logo-grey.svg" alt="Spare Keys Logo">
									</div>								
								</div>
								
								<span class="block">Logo Variation One</span>
								<small class="block">Includes .ai, .eps, .jpg, and .png</small>
								
								<br />
							
								<a href="#" class="button blue">Download</a>
								
							</div><!-- .brand-block -->
						
					</div>
				</div><!-- .col -->
				<div class="col col-4 sm-col-2">
					<div class="item">
					
							<div class="brand-block">
							
								<div class="bordered-item">
									<div class="pad-20">
										<img src="../assets/dist/images/spare-keys-logo-grey.svg" alt="Spare Keys Logo">
									</div>								
								</div>
								
								<span class="block">Logo Variation One</span>
								<small class="block">Includes .ai, .eps, .jpg, and .png</small>
								
								<br />
							
								<a href="#" class="button blue">Download</a>
								
							</div><!-- .brand-block -->
						
					</div>
				</div><!-- .col -->
				<div class="col col-4 sm-col-2">
					<div class="item">
					
							<div class="brand-block">
							
								<div class="bordered-item">
									<div class="pad-20">
										<img src="../assets/dist/images/spare-keys-logo-grey.svg" alt="Spare Keys Logo">
									</div>								
								</div>
								
								<span class="block">Logo Variation One</span>
								<small class="block">Includes .ai, .eps, .jpg, and .png</small>
								
								<br />
							
								<a href="#" class="button blue">Download</a>
								
							</div><!-- .brand-block -->
						
					</div>
				</div><!-- .col -->
				<div class="col col-4 sm-col-2">
					<div class="item">
					
							<div class="brand-block">
							
								<div class="bordered-item">
									<div class="pad-20">
										<img src="../assets/dist/images/spare-keys-logo-grey.svg" alt="Spare Keys Logo">
									</div>								
								</div>
								
								<span class="block">Logo Variation One</span>
								<small class="block">Includes .ai, .eps, .jpg, and .png</small>
								
								<br />
							
								<a href="#" class="button blue">Download</a>
								
							</div><!-- .brand-block -->
						
					</div>
				</div><!-- .col -->
			</div><!-- .grid -->

		</section>
		
		<hr />
		
		<section>
			
			<div class="halved-grid">
			
				<div class="grid center eqh">
				
					<div class="col-2 col sm-col-1">
					
						<div class="item">
							
							<div class="section-title">
								<h2 class="title">Brand Guidelines</h2>
							</div><!-- .section-title -->
								
							<div class="brand-block">
							
								<div class="bordered-item">
									<div class="pad-20">
										<img src="../assets/dist/images/spare-keys-logo-grey.svg" alt="Spare Keys Logo">
									</div>								
								</div>
								
								<span class="block">Brand Guidelines</span>
								<small class="block">Available in .pdf format</small>
								
								<br />
							
								<a href="#" class="button blue">Download</a>
								
							</div><!-- .brand-block -->
								
						</div><!-- .item -->
						
					</div><!-- .col -->
					
					<div class="col-2 col sm-col-1">
					
						<div class="item">

							<div class="section-title">
								<h2>Contact</h2>
							</div><!-- .section-title -->
							
							<div class="ar" data-ar="40">
								<iframe class="ar-child" frameborder="0" style="border:0" src="https://www.google.com/maps/embed/v1/place?key=<?php echo $GOOGLE_MAPS_API_KEY; ?>&q=Mount Pearl, NL"></iframe>
							</div>
							
							<br />
							
							<address>
								PO Box 3055 Pealgate Post Office <br />
								Mount Pearl, NL
							</address>
							
							<br />
							
							<span class="block">T: 1-709-764-8888</span>
								
						</div><!-- .item -->
						
					</div><!-- .col -->
					
				</div><!-- .grid -->
			
			</div><!-- .halved-grid -->
			
		</section>
		
	</div><!-- .body -->
</div><!-- .sw -->


<?php include('inc/i-footer.php'); ?>