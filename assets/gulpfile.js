var gulp = require('gulp');
var sass = require('gulp-sass');
var cssShrink = require('gulp-cssshrink');
var bower = require('gulp-bower');
var uglify = require('gulp-uglify');
var imagemin = require('gulp-imagemin');
var pngquant = require('imagemin-pngquant');
var browserify = require('browserify');
var vtransform = require('vinyl-transform');

var tasks = {

	css: function(opts) {
		var st;
		st = gulp.src('./src/css/**/style.scss');
		st = st.pipe(sass({errLogToConsole:true}))
		
		if(opts && opts.shrink) {
			st = st.pipe(cssShrink());
		}
		
		st = st.pipe(gulp.dest('./dist/css'));
		return st;
	},
	
	images: function(opts) {
		var st;
	
		st = gulp.src([
				'./src/images/**/*.jpg',
				'./src/images/**/*.png',
				'./src/images/**/*.svg',
				'./src/images/**/*.ico'
			])
			
		if(opts && opts.min) {
			st = st.pipe(imagemin({
					progressive: true,
					svgoPlugins: [{removeViewBox: false}],
					use:[pngquant()]
				}))
		}
		
		st = st.pipe(gulp.dest('./dist/images'));
		return st;
	},
	
	javascript: function(opts) {
		var st;
		var browserified = vtransform(function(filename) {
			return browserify(filename)
				.bundle()
				.on('error',function(err) { 
					console.log(err.message);
					this.emit('end');
				});
		});
		
		
		st = gulp.src(['./src/js/main.js']).pipe(browserified);
		
		if(opts && opts.min) {
			st = st.pipe(uglify());
		}
		
		st = st.pipe(gulp.dest('./dist/js'));	
		return st;
	},
	
	fonts: function() {
		var st;
		
		st = gulp.src('./src/fonts/**/*.*');
		st = st.pipe(gulp.dest('dist/fonts'));
		return st;
	},
	
	bower: function(opts) {
		//installs bower components in bower.json
		bower();
	
		//move the specified files to the dist/lib directory
		gulp
			.src([
				'./bower_components/modernizr/modernizr.js',
				'./bower_components/jquery/dist/jquery.min.js',
				'./bower_components/slick.js/slick/**/*'
			],{
				base: 'bower_components'
			})
			.pipe(gulp.dest('./dist/lib'));
	},
	
	watch: function() {
		
		//sass
		gulp.watch('src/css/**/*.scss',['css']);
		
		//images
		gulp.watch([
				'src/images/**/*.jpg',
				'src/images/**/*.png',
				'src/images/**/*.svg',
				'src/images/**/*.ico'
		],['images']);
		
		//javascript
		gulp.watch('src/js/**/*.js',['js']);
		
		//bower
		gulp.watch(['bower_components/**/*.css','bower_components/**/*.js'],['bower']);
		
		//fonts
		gulp.watch('src/fonts/**/*',['fonts']);
		
	},
	
	clean: function() {
		//clean dist
	},
	
	dev: function() {
		tasks.clean();
		tasks.bower();
		tasks.css({});
		tasks.javascript({});
		tasks.fonts();
		tasks.images({});
		tasks.watch();
	},
	
	dist: function() {
		tasks.clean();
		tasks.bower();
		tasks.css({shrink:true});
		tasks.javascript({min:true});
		tasks.images({min:true});	
		tasks.fonts();
	}
	
};
	
gulp.task('default',tasks.dev);
gulp.task('dist',tasks.dist);
gulp.task('dev',tasks.dev);
gulp.task('css',tasks.css);
gulp.task('js',tasks.javascript);
gulp.task('fonts',tasks.fonts);
gulp.task('images',tasks.images);
gulp.task('bower',tasks.bower);
gulp.task('watch',tasks.watch);