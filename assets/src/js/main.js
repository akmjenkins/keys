//load all required scripts
require('./scripts/anchors.external.popup.js');
require('./scripts/standard.accordion.js');
require('./scripts/custom.select.js');
require('./scripts/aspect.ratio.js');
require('./scripts/lazy.images.js');
require('./scripts/tabs.js');
require('./scripts/nav.js');

var preventOverScroll = require('./scripts/preventOverScroll.js');
preventOverScroll($('div.nav')[0]);


$('.hero-wrap').each(function() {
	var el = $(this);
	var ctrls = $('.hero-nav .btn');
	var isHome = $('body').hasClass('home');
	var slickEl = el.slick({
		centerMode: true,
		slidesToShow:1,
		variableWidth:true,
		centerPadding:0,
		focusOnSelect: isHome,
		draggable:false,
		swipe:isHome,
		touchMove:isHome,
		autoplay: isHome,
		autoplaySpeed: 5000,
		pauseOnHover: false,
		responsive: [
			{
				breakpoint: 1024,
				variableWidth: false
			}
		],
		onBeforeChange: function(slick,e,i) {
			ctrls
				.removeClass('selected')
				.eq(i)
				.addClass('selected');
		}
	});
	
	ctrls
		.on('click',function() {
			slickEl.slickGoTo($(this).index());
		});
});