var debounce = require('./debounce.js');
var tests = require('./tests.js');

module.exports = (function() {

	var 
		scrollDebounce = debounce(),
		resizeDebounce = debounce(),
		$window = $(window),
		$document = $(document),
		$html = $('html'),
		$nav = $('nav'),
		$navWrap  =$('div.nav'),
		$body = $('body'),
		$header = $('header'),
		startingHeaderOffset = $header.height() + $header.offset().top,
		$searchForm = $('.search-form',$nav),
		$searchInput = $('input',$searchForm),
		$pageWrapper = $('div.page-wrapper'),
		startingNavOffset = $nav.height()+$nav.offset().top,
		SHOW_SEARCH_CLASS = 'show-search',
		SHOW_CLASS = 'show-nav',
		SMALL_NAV = 'small-nav',
		SMALL_HEADER = 'small-header',
		COLLAPSE_NAV_AT = 750,
		COLLAPSE_SEARCH_AT = 900;

	var methods = {
	
		checkShowSmallNav: function() {
			
			if(!this.isNavCollapsed() && $window.scrollTop() > startingNavOffset) {
				$body.addClass(SMALL_NAV);
			} else {
				$body.removeClass(SMALL_NAV);
			}
		},
		
		checkShowSmallHeader: function() {
			if($window.scrollTop() > startingHeaderOffset) {
				$body.addClass(SMALL_HEADER);
			} else {
				$body.removeClass(SMALL_HEADER);
			}
		},
		
		isNavCollapsed: function() {
			return $window[0].innerWidth < COLLAPSE_NAV_AT;
		},

		onScroll: function() {
			this.checkShowSmallNav();
		},
		
		onResize: function() {
			this.checkShowSmallNav();
		},
	
		showNav: function(show) {
			$html[show ? 'addClass' : 'removeClass'](SHOW_CLASS);
		},

		toggleNav: function() {
			this.showNav(!this.isShowingNav());
		},

		isShowingNav: function() {
			return $html.hasClass(SHOW_CLASS);
		},
		
		showSearch: function(show) {
			$html[show ? 'addClass' : 'removeClass'](SHOW_SEARCH_CLASS);
			show && $('input',$searchForm).focus();
		},
		
		toggleSearch: function() {
			this.showSearch(!this.isShowingSearch());
		},
		
		isShowingSearch: function() {
			return $html.hasClass(SHOW_SEARCH_CLASS) || ($window[0].innerWidth > COLLAPSE_SEARCH_AT && !this.isNavCollapsed());
		}

	};
	
	//listeners
	$document
		.on('click','.toggle-nav',function(e) {
			methods.toggleNav();
			return false;
		})
		.on('keydown',function(e) {
			if(e.result !== false && e.keyCode === 27) {
				if(methods.isShowingSearch()) {
					methods.showSearch(false);
					return false;
				}
				
				if(methods.isShowingNav()) {
					methods.showNav(false);
					return false
				}
			}
		})
		.on('click','.mobile-nav-bg',function() {
			methods.showNav(false);
		})
		.on('submit','.nav .search-form',function(e) {
			if(!methods.isShowingSearch()) {
				e.preventDefault();
				methods.showSearch(true);
			}
		})
		.on('click','.nav .search-form .close',function() {
			methods.showSearch(false);
		})

	$window
		.on('scroll',function() {
			scrollDebounce.requestProcess(methods.onScroll,methods);
		})
		.on('resize',function() {
			resizeDebounce.requestProcess(methods.onResize,methods);
		});
		
		if(tests.ios()) {
			
			//onScroll must be fired continuously
			(function iOSOnScroll() {
				methods.onScroll();
				requestAnimationFrame(function() { iOSOnScroll(); });
			}());
		
		} else {
			//fire immediately
			methods.onScroll();
			methods.onResize();
		}

}());