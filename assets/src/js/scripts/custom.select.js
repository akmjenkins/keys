module.exports = (function() {

	var methods = {
	
		update: function() {
			var 
				el = $(this),
				select = $('select',el),
				val = $('span.value',el),
				selectedOption = select.find('option:selected'),
				valText = selectedOption.data('tag') || selectedOption.text();

			el.data('selector',{update:methods.update.bind(el)});
			val.html(valText);
		}
	
	}

	//selector wrapper
	$(document)
		.on('change','div.selector',function(e) {
			methods.update.apply(this);
		}).on('updateTemplate.selector',function() {
			$('div.selector').each(function() { methods.update.apply(this); });
		});

	//setup on page load
	$('div.selector').each(function() { methods.update.apply(this); });
	
	
	//no public API
	return {};
	
}());