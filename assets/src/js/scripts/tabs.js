module.exports = (function() {
	
		var $document = $(document);

		$document
			.on('change','select.tab-controller',function(e) {

				var
					el = $(this),
					wrapper = el.closest('div.tab-wrapper'),
					tabControlWrapper = wrapper.children('div.tab-controls'),
					controls = tabControlWrapper.find('.tab-control'),
					holder = wrapper.children('div.tab-holder'),
					tabs = holder.children('div.tab'),
					tabToShow = tabs.eq(this.selectedIndex);

				if(controls) {
					controls.removeClass('selected').eq(tabToShow.index()).addClass('selected');
				}

				tabs.removeClass('selected');
				tabToShow.addClass('selected');
				
				//fire event
				tabToShow.trigger('tabChanged',[tabToShow]);

			})
			.on('click','.tab-control',function(e) {
				e.preventDefault();

				var
					el = $(this),
					wrapper = el.closest('div.tab-wrapper'),
					tabControlWrapper = wrapper.children('div.tab-controls'),
					controls = tabControlWrapper.find('.tab-control'),
					selectorControl = tabControlWrapper.find('select.tab-controller'),
					holder = wrapper.children('div.tab-holder'),
					tabs = holder.children('div.tab'),
					tabToShow;
			
				
				if(el.data('selector')) {
					tabToShow = $(el.data('selector'));
				}
				
				tabToShow = (tabToShow && tabToShow.length) ? tabToShow : tabs.eq(controls.index(el));
				
				if(selectorControl.length) {
					selectorControl[0].selectedIndex = tabToShow.index();
					selectorControl.trigger('change');
				} else {
					controls.removeClass('selected').eq(tabToShow.index()).addClass('selected');
					tabs.removeClass('selected');
					tabToShow.addClass('selected');					
					
					//fire event
					tabToShow.trigger('tabChanged',[tabToShow]);
				}
				
			});
			
		$document.on('tabChanged',function(e,el) { $document.trigger('updateTemplate'); });
			
	//no public API	
	return {};
}());