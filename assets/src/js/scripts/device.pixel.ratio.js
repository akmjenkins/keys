module.exports = function() {

	if(window.devicePixelRatio) {
	
		if(window.devicePixelRatio > 2 ) { //i.e. 2.5
			$('html').addClass('threex');
			
		} else if(window.devicePixelRatio > 1) { //i.e. 1.5
			$('html').addClass('twox');
		}
	}

	//no public API
	return {};
}());