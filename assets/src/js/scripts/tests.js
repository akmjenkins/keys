//rather than using Modernizr, which is big and bulky and unnecessary seeing how we only support modern browsers now anyway
module.exports = function() {

	var $html = $('html');

	var tests = {
		ios: function() {
			return window.navigator.userAgent.match(/iphone|ipad/i);
		}	
	};

	//add these to the HTML tag, like modernizr would
	$html.addClass(tests.ios() ? 'ios' : 'no-ios');
	
	return tests;

}();